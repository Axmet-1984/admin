﻿using admin.ModelContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace admin.Data
{
    public static class DbInitializer
    {
        public static void Initializer(AdminContext context)
        {
            context.Database.EnsureCreated();
            if (context.Users.Any())
            {
                return;
            }
            var users = new User[]
                {
                    new User { UserName = "Erkin"},
                    new User { UserName = "Rus"},
                    new User{ UserName = "Gibra"},
                    new User{ UserName = "Tomas"},
                    new User { UserName = "Saken"},
                    new User { UserName = "Rustik Pennywise"}
                };
            foreach (User u in users)
            {
                context.Users.Add(u);
            }
            context.SaveChanges();

            var debugs = new Debug[]
                {
                    new Debug { DebugName="Отсутствие локльной сети"},
                    new Debug { DebugName="Не работает пресональный компьютер"},
                    new Debug { DebugName = "Не печатает принтер, ошибка драйвера , нет подключения"},
                    new Debug { DebugName="Установка нового или обновления существующего ПО"},
                    new Debug { DebugName="Замена картриджа"},
                    new Debug { DebugName="Консультация по телефону, устновка нового пороля, сбос блокировки пользователя"}
                };
            foreach (Debug d in debugs)
            {
                context.Debugs.Add(d);
            }
            context.SaveChanges();

            var debugReport = new DebugReports[]
                {
                    new DebugReports { DebugId = debugs[2].Id},
                    new DebugReports { UserId = users[2].Id},
                    new DebugReports { Phone = "555555"},
                    new DebugReports { Cabinet = "B1.2.14"},
                    new DebugReports { TimeStartWork = DateTimeOffset.Now}
                };

    
        }
    }
}
