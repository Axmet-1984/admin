﻿using System;
using System.Collections.Generic;
using System.Text;

namespace admin.ModelContext
{
    public class Debug
    {
        public Guid Id { get; set; }
        public string DebugName { get; set; }

        public ICollection<DebugReports> DebugReports { get; set; }
    }
}
