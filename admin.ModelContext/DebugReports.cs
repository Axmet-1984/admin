﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace admin.ModelContext
{
    public class DebugReports
    {
        public Guid Id { get; set; }
        public Guid DebugId { get; set; }
        public Guid UserId { get; set; }
        [Phone]
        public string Phone { get; set; }
        public string Cabinet { get; set; }




        public DateTimeOffset TimeStartWork { get; set; }

        public DateTimeOffset TimeOverWork { get; set; }

        public string Commentary { get; set; }

        public User User { get; set; }
        public Debug Debug { get; set; }
    }
}
