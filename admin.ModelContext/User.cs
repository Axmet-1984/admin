﻿using System;
using System.Collections.Generic;
using System.Text;

namespace admin.ModelContext
{
    public class User
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }

        public ICollection<DebugReports> DebugReports { get; set; }
    }
}
