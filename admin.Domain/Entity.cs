﻿using System;
using System.Collections.Generic;
using System.Text;
using admin.Domain.Enums;

namespace admin.Domain
{
    public abstract class Entity : IEntity
    {
        public Guid Id { get;set;}
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public EntityState State { get; set; }

        public Entity()
        {
            CreatedAt = GetCurrentDate();
        }


        private DateTimeOffset GetCurrentDate()
        {
            return DateTimeOffset.Now;
        }
    }
}
