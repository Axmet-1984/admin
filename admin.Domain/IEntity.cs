﻿using admin.Domain.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace admin.Domain
{
    public interface IEntity
    {
        Guid Id { get; set; }
        DateTimeOffset CreatedAt { get; set; }
        DateTimeOffset ModifiedAt { get; set; }
        EntityState State { get; set; }

    }
}
